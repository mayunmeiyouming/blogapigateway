package generated

// THIS CODE IS A STARTING POINT ONLY. IT WILL NOT BE UPDATED WITH SCHEMA CHANGES.

import (
	"context"

	"gitlab.com/mayunmeiyouming/blog/rpc/blog"
	"gitlab.com/mayunmeiyouming/blogapigateway/internal/models"
	"google.golang.org/protobuf/types/known/wrapperspb"
)

type Resolver struct{}

func (r *mutationResolver) CreateArticleFunc(ctx context.Context, create models.CreateArticle) (*blog.Article, error) {
	panic("not implemented")
}

func (r *mutationResolver) DeleteArticleFunc(ctx context.Context, del *models.DeleteArticle) (*wrapperspb.BoolValue, error) {
	panic("not implemented")
}

func (r *mutationResolver) UpdateArticleFunc(ctx context.Context, update *models.UpdateArticle) (*blog.Article, error) {
	panic("not implemented")
}

func (r *mutationResolver) CreateCommentFunc(ctx context.Context, create *models.CreateComment) (*blog.Comment, error) {
	panic("not implemented")
}

func (r *mutationResolver) DeleteCommentFunc(ctx context.Context, del *models.DeleteComment) (*wrapperspb.BoolValue, error) {
	panic("not implemented")
}

func (r *mutationResolver) UpdateCommentFunc(ctx context.Context, update *models.UpdateComment) (*blog.Comment, error) {
	panic("not implemented")
}

func (r *mutationResolver) CreateUserFunc(ctx context.Context, create *models.CreateUser) (*blog.User, error) {
	panic("not implemented")
}

func (r *queryResolver) GetArticlesFunc(ctx context.Context, first *wrapperspb.Int32Value, after *wrapperspb.Int32Value, last *wrapperspb.Int32Value, before *wrapperspb.Int32Value, state *models.ArticleState, authorID *int64, user *models.RequestUser) (*blog.ArticlesConnection, error) {
	panic("not implemented")
}

func (r *queryResolver) GetArticleFunc(ctx context.Context, request *models.GetArticleRequest) (*blog.Article, error) {
	panic("not implemented")
}

func (r *queryResolver) GetCommentFunc(ctx context.Context, request *models.GetCommentRequest) (*blog.GetCommentResponse, error) {
	panic("not implemented")
}

func (r *queryResolver) GetArticleCountFunc(ctx context.Context, request *models.ArticleCountRequest) (*blog.ArticleCountResponse, error) {
	panic("not implemented")
}

// Mutation returns MutationResolver implementation.
func (r *Resolver) Mutation() MutationResolver { return &mutationResolver{r} }

// Query returns QueryResolver implementation.
func (r *Resolver) Query() QueryResolver { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
