package models

import (
	"fmt"
	"io"

	timestamp "google.golang.org/protobuf/types/known/timestamppb"
	"github.com/99designs/gqlgen/graphql"
	"strconv"
	"time"
)

func MarshalTimeStamp(timestamp timestamp.Timestamp) graphql.Marshaler {
	if timestamp.Seconds == 0 {
		return nil
	}

	date := time.Unix(timestamp.Seconds, 0)

	return graphql.WriterFunc(func(w io.Writer) {
		io.WriteString(w, strconv.Quote(fmt.Sprint(date)))
	})
}
func UnmarshalTimeStamp(v interface{}) (timestamp.Timestamp, error) {
	if v, ok := v.(string); ok {
		const base_format = "2006-01-02 15:04:05"
		t, _ := time.Parse(base_format, v)
		datetime_str_to_timestamp := t.Unix()
		return timestamp.Timestamp {
			Seconds: datetime_str_to_timestamp,
		}, nil
	}
	return timestamp.Timestamp{}, nil
}


