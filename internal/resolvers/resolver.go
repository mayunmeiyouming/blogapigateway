package resolvers

// THIS CODE IS A STARTING POINT ONLY. IT WILL NOT BE UPDATED WITH SCHEMA CHANGES.

import (
	"context"
	"log"

	"gitlab.com/mayunmeiyouming/blog/rpc/blog"
	"gitlab.com/mayunmeiyouming/blogapigateway/internal/models"
	"google.golang.org/protobuf/types/known/wrapperspb"
)

type Resolver struct{
	Client blog.TodoTwirp 
}

func (r *mutationResolver) CreateArticleFunc(ctx context.Context, create models.CreateArticle) (*blog.Article, error) {
	loginUser := &blog.RequestUser {
		UserId: create.User.UserID,
		Password: create.User.Password,
	}

	article, err := r.Client.CreateArticleFunc(
		context.Background(),
		&blog.CreateArticle{
			User: loginUser,      
			Text: create.Text,             
			State: create.State,
		},
	)

	if err != nil {
		log.Fatal(err)
	}

	return article, nil
}

func (r *mutationResolver) DeleteArticleFunc(ctx context.Context, del *models.DeleteArticle) (*wrapperspb.BoolValue, error) {
	loginUser := &blog.RequestUser {
		UserId: del.User.UserID,
		Password: del.User.Password,
	}

	_, err := r.Client.DeleteArticleFunc(
		context.Background(), 
		&blog.DeleteArticle {
			User: loginUser,
			ArticleId: del.ArticleID,
		},
	)
	if err != nil {
		log.Println(err)
	}

	return &wrapperspb.BoolValue {Value: true}, nil
}

func (r *mutationResolver) UpdateArticleFunc(ctx context.Context, update *models.UpdateArticle) (*blog.Article, error) {
	loginUser := &blog.RequestUser {
		UserId: update.User.UserID,
		Password: update.User.Password,
	}

	article, err := r.Client.UpdateArticleFunc(
		context.Background(), 
		&blog.UpdateArticle {
			User: loginUser,
			ArticleId: update.ArticleID,
			Text: &blog.TextOptional {
				Text: *update.Text,
			},
			State: &blog.StateOptional {
				State: *update.State,
			},
		},
	)
	if err != nil {
		log.Println(err)
	}

	return article, nil
}

func (r *mutationResolver) CreateCommentFunc(ctx context.Context, create *models.CreateComment) (*blog.Comment, error) {
	loginUser := &blog.RequestUser {
		UserId: create.User.UserID,
		Password: create.User.Password,
	}

	comment, err := r.Client.CreateCommentFunc(
		context.Background(),
		&blog.CreateComment{
			User: loginUser,  
			ArticleId: create.ArticleID,
			Text: create.Text,             
		},
	)

	if err != nil {
		log.Fatal(err)
	}

	return comment, nil
}

func (r *mutationResolver) DeleteCommentFunc(ctx context.Context, del *models.DeleteComment) (*wrapperspb.BoolValue, error) {
	loginUser := &blog.RequestUser {
		UserId: del.User.UserID,
		Password: del.User.Password,
	}

	_, err := r.Client.DeleteCommentFunc(
		context.Background(), 
		&blog.DeleteComment {
			User: loginUser,
			CommentId: del.CommentID,
		},
	)
	if err != nil {
		log.Println(err)
	}

	return &wrapperspb.BoolValue {Value: true}, nil
}

func (r *mutationResolver) UpdateCommentFunc(ctx context.Context, update *models.UpdateComment) (*blog.Comment, error) {
	loginUser := &blog.RequestUser {
		UserId: update.User.UserID,
		Password: update.User.Password,
	}

	comment, err := r.Client.UpdateCommentFunc(
		context.Background(), 
		&blog.UpdateComment{
			User: loginUser,
			CommentId: update.CommentID,
			Text: update.Text,
		},
	)
	if err != nil {
		log.Println(err)
	}

	return comment, nil
}

func (r *mutationResolver) CreateUserFunc(ctx context.Context, create *models.CreateUser) (*blog.User, error) {
	user, err := r.Client.CreateUserFunc(
		context.Background(), 
		&blog.CreateUser {
			Name: create.Name,
			Password: create.Password,
		},
	)

	if err != nil {
		log.Fatal(err)
	}
	return user, nil
}

func (r *queryResolver) GetArticlesFunc(
	ctx context.Context, 
	first *wrapperspb.Int32Value, 
	after *wrapperspb.Int32Value, 
	last *wrapperspb.Int32Value, 
	before *wrapperspb.Int32Value, 
	state *models.ArticleState, 
	authorID *int64, 
	user *models.RequestUser,
	) (*blog.ArticlesConnection, error) {

	loginUser := &blog.RequestUser {
		UserId: user.UserID,
		Password: user.Password,
	}

	var stateOptional blog.ArticleState
	
	if *state == models.ArticleStatePublished {
		stateOptional = blog.ArticleState_PUBLISHED
	} else if *state == models.ArticleStateDraft {
		stateOptional = blog.ArticleState_DRAFT
	} else {
		stateOptional = blog.ArticleState_TRASH
	}

	// google.protobuf.Int32Value first = 1;
    // google.protobuf.Int32Value after = 2;
    // google.protobuf.Int32Value last = 3;
    // google.protobuf.Int32Value before = 4;
    // ArticleState state = 5;
    // int64 authorID = 6;
    // RequestUser user = 8;

	articlesResponse, err := r.Client.GetArticlesFunc(
		context.Background(), 
		&blog.GetArticlesRequest {
			User: loginUser,
			First: first,
			After: after,
			Last: last,
			Before: before,
			AuthorID: *authorID,
			State: stateOptional,
		},
	)
	if err != nil {
		log.Fatal(err)
	}

	return articlesResponse, nil
}

func (r *queryResolver) GetArticleFunc(ctx context.Context, request *models.GetArticleRequest) (*blog.Article, error) {
	loginUser := &blog.RequestUser {
		UserId: request.User.UserID,
		Password: request.User.Password,
	}

	article, err := r.Client.GetArticleFunc(
		context.Background(), 
		&blog.GetArticleRequest {
			User: loginUser,
			ArticleID: *request.ArticleID,
		},
	)

	if err != nil {
		log.Fatal(err)
	}

	return article, nil
}

func (r *queryResolver) GetCommentFunc(ctx context.Context, request *models.GetCommentRequest) (*blog.GetCommentResponse, error) {
	loginUser := &blog.RequestUser {
		UserId: request.User.UserID,
		Password: request.User.Password,
	}

	var commentIDOptional *blog.CommentIdOptional
	if request.CommentID != nil {
		commentIDOptional.CommentId = *request.CommentID
	}
	commentsResponse, err := r.Client.GetCommentFunc(
		context.Background(), 
		&blog.GetCommentRequest {
			User: loginUser,
			CommentIdOptional: commentIDOptional,
			AuthorId: *request.AuthorID,
		},
	)

	return commentsResponse, err
}

func (r *queryResolver) GetArticleCountFunc(ctx context.Context, request *models.ArticleCountRequest) (*blog.ArticleCountResponse, error) {
	var getAuthors blog.GetAuthors
	if request.GetAuthors == models.GetAuthorsAll {
		getAuthors = blog.GetAuthors_ALL
	} else {
		getAuthors = blog.GetAuthors_ONE
	}
	articleCount, err := r.Client.GetArticleCountFunc(
		context.Background(), 
		&blog.ArticleCountRequest {
			GetAuthors: getAuthors,
			UserIdOptional: &blog.UserIDOptional {
				UserId: *request.UserID,
			},
		},
	)
	if err != nil {
		log.Fatal(err)
	}
	return articleCount, nil
}

// Mutation returns MutationResolver implementation.
func (r *Resolver) Mutation() MutationResolver { return &mutationResolver{r} }

// Query returns QueryResolver implementation.
func (r *Resolver) Query() QueryResolver { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
