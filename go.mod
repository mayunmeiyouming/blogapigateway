module gitlab.com/mayunmeiyouming/blogapigateway

go 1.14

require (
	github.com/99designs/gqlgen v0.11.3
	github.com/golang/protobuf v1.4.2
	github.com/vektah/gqlparser/v2 v2.0.1
	gitlab.com/mayunmeiyouming/blog v0.0.0
	google.golang.org/protobuf v1.25.0
)

replace gitlab.com/mayunmeiyouming/blog => /home/pacman/桌面/blog/blog
