mutation createUser {
  CreateUserFunc(
    create: {
      name: "黄伟"
      password: "123456"
    }
  ) {
    name
    userId
    password
  }
}

mutation createArticle1 {
  CreateArticleFunc(
    create: {
      user: {
        userID: 1,
        password: "123456"
      }
      text: "content 1"
      state: true
    }
  ) {
    user {
      name
      userId
    }
    updateTime
    createTime
    text
    state
  }
}

mutation createArticle2 {
  CreateArticleFunc(
    create: {
      user: {
        userID: 1,
        password: "123456"
      }
      text: "content 2"
      state: true
    }
  ) {
    user {
      name
      userId
    }
    updateTime
    createTime
    text
    state
  }
}

mutation createArticle3 {
  CreateArticleFunc(
    create: {
      user: {
        userID: 1,
        password: "123456"
      }
      text: "content 3"
      state: false
    }
  ) {
    user {
      name
      userId
    }
    updateTime
    createTime
    text
    state
  }
}

mutation createArticle4 {
  CreateArticleFunc(
    create: {
      user: {
        userID: 1,
        password: "123456"
      }
      text: "content 4"
      state: false
    }
  ) {
    user {
      name
      userId
    }
    updateTime
    createTime
    text
    state
  }
}

query findArticles {
  GetArticlesFunc(
    last: 2
    before: 0
    authorID: 1
    user: {
      userID: 1
      password: "123456"
    }
    state: PUBLISHED
  ) {
    nodes {
       user {
        name
        userId
      }
      
      updateTime
      createTime
      text
      state
      articleID
    }
    edges {
      cursor
    }
  }
}

mutation updateArticle1 {
  UpdateArticleFunc(
    update: {
      user: {
        userID: 1
        password: "123456"
      }
      article_id: 1
      text: "update article 1"
      state: true
    }
  ) {
    user {
      name
      userId
    }
    updateTime
    createTime
    text
    state
  }
}

mutation deleteArticle1 {
  DeleteArticleFunc(
    del: {
      user: {
        userID: 1
        password: "123456"
      }
      articleID: 2
    }
  ) 
}

mutation createComment1 {
  CreateCommentFunc(
    create: {
      user: {
        userID: 1,
        password: "123456"
      }
      text: "comment 1"
      articleID: 1
    }
  ) {
    user {
      name
      userId
    }
    text
    articleID
  }
}

mutation createComment2 {
  CreateCommentFunc(
    create: {
      user: {
        userID: 1,
        password: "123456"
      }
      text: "comment 2"
      articleID: 1
    }
  ) {
    user {
      name
      userId
    }
    text
    articleID
  }
}

mutation updateComment1 {
  UpdateCommentFunc(
    update: {
      user: {
        userID: 1
        password: "123456"
      }
      commentID: 1
      text: "update comment 1"
    }
  ) {
    user {
      name
      userId
    }
    text
    commentID
  }
}

mutation deleteComment1 {
  DeleteCommentFunc(
    del: {
      user: {
        userID: 1
        password: "123456"
      }
      commentID: 2
    }
  ) 
}

query findComments {
  GetCommentFunc(
    request: {
      user: {
        userID: 1
        password: "123456"
      }
      authorID: 1
    }
  ) {
    comment{
      text
      timeStamp
      articleID
      commentID
    }
  }
}